<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\AlumniController;
use App\Http\Controllers\PengajuanController;
use App\Http\Controllers\PostingController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\PerusahaanController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\User1Controller;
use App\Http\Controllers\HomeController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('login', function () {
    return view('footer.login');
})->name('login');



Route::get('beranda', function () {
    return view('layout.landingpage');
});

Route::get('dashboard', function () {
    return view('layout.dashboard');
});

Route::get('form', function () {
    return view('inp');
});


Route::get('lading', [HomeController::class, 'show']);
Route::get('stisla', [HomeController::class, 'admin']);
Route::get('index', [HomeController::class, 'from']);
Route::get('tmb', [HomeController::class, 'index']);

// Buat Akun Pengguna
Route::post('/login', [User1Controller::class, 'show']);
Route::get('/daftar', [User1Controller::class, 'show']);
Route::post('/daftar/create', [User1Controller::class, 'create']);
Route::post('/auth', [User1Controller::class, 'auth']);
Route::get('/logout', [User1Controller::class, 'logout']);


// Perusahaan
Route::get('/perusahaan', [PerusahaanController::class, 'show']);
Route::get('/perusahaan/add', [PerusahaanController::class, 'add']);
Route::post('/perusahaan/create', [PerusahaanController::class, 'create']);
Route::get('/perusahaan/edit/{id}', [PerusahaanController::class, 'edit']);
Route::post('/perusahaan/update/{id}', [PerusahaanController::class, 'update']);
Route::get('/perusahaan/hapus/{id}', [PerusahaanController::class, 'hapus']);

// Posting
Route::get('/posting', [PostingController::class, 'show']);
Route::get('/posting/add', [PostingController::class, 'add']);
Route::post('/posting/create', [PostingController::class, 'create']);
Route::get('/posting/edit/{id}', [PostingController::class, 'edit']);
Route::post('/posting/update/{id}', [PostingController::class, 'update']);
Route::get('/posting/hapus/{id}', [PostingController::class, 'hapus']);

// Pengajuan
Route::get('/admin', [PengajuanController::class, 'adm']);

Route::get('/pengajuan', [PengajuanController::class, 'show']);
Route::get('/pengajuan/add', [PengajuanController::class, 'add']);
Route::post('/pengajuan/create', [PengajuanController::class, 'create']);
Route::get('/pengajuan/edit/{id}', [PengajuanController::class, 'edit']);
Route::post('/pengajuan/update/{id}', [PengajuanController::class, 'update']);
Route::get('/pengajuan/hapus/{id}', [PengajuanController::class, 'hapus']);