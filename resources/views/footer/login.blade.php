@extends('auth')

@section('title', 'Alumni-Login')

@section('content')

<section>
    <div class="form-box">
        <div class="form-value">
            <form action="">
              <!-- <img src="img/chat.png" alt="100"  width="100" height="100"> -->
                <div class="login-group">
                    <div class="login-count">
                        {{-- <img src="img/logo smk.png" alt="100"  width="50" height="50"> --}}
                        <h2>LOGIN</h2></div>             
                        <form method="post" action="/auth">
                        @csrf
                <div class="inputbox">
                    <ion-icon name="mail-outline"></ion-icon>
                    <input type="email" required>
                    <label for="">Email</label>
                </div>
                <div class="inputbox">
                    <ion-icon name="lock-closed-outline"></ion-icon>
                    <input type="password" required>
                    <label for="">Password</label>
                </div>
                {{-- <div class="inputbox">
                    <ion-icon name="speedometer-outline"></ion-icon>
                    <input type="text" required>
                    <div class="select-style" type="Level">
                        <select name="role" id="role">
                            <option value="admin">Admin</option>
                            <option value="user">Pengguna</option>
                        </select>
                    </div>
                </div> --}}
                <!-- <div class="forget">
                    <label for=""><input type="checkbox">Remember Me</label> <a href="#">Forget Password</a>
                   
                </div> -->
                <button>Log In</button>
                <div class="register">
                    <p>Don't have a account</p><a href="daftar">Register</a>
                </div>
            </form>
        </div>
    </div>
</section>
<script type="module" src="https://unpkg.com/ionicons@7.1.0/dist/ionicons/ionicons.esm.js"></script>
<script nomodule src="https://unpkg.com/ionicons@7.1.0/dist/ionicons/ionicons.js"></script>
    
@endsection