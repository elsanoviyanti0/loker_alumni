<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Posting;

class PostingController extends Controller
{
    //
     function show(){
      $data['posting'] = Posting::all();
        return view('posting.index',$data);
     }

     function add(){
        $data=[
            'action' => url('posting/create'),
            'tombol' =>'Simpan',
            'posting' =>(object)[
                'nama' =>'',
                'bidang_usaha' =>'',
                 'persyaratan' =>'',
                 'lowongan' =>'',
                 'ttl_p' =>'',
                 'ttl_tp' =>'',
                 'deskripsi' =>'',
                 'foto' =>'',
                 'lokasi' =>'',
            ]
            ];
            return view('posting.from',$data);
     }

     function create(Request $req){
        Posting::create([
                 'nama' =>$req->nama,
                 'bidang_usaha' =>$req->bidang_usaha,
                 'persyaratan' =>$req->persyaratan,
                 'lowongan' =>$req->lowongan,
                 'ttl_p' =>$req->ttl_p,
                 'ttl_tp' =>$req->ttl_tp,
                 'deskripsi' =>$req->deskripsi,
                 'foto' =>$req->foto,
                 'lokasi' =>$req->lokasi,
        ]);
        return redirect('posting.index');
     }

     function hapus($id){
        $posting = Posting::where('id',$id)->delete();
        return redirect('posting.index');
     }

     function edit($id){
        $data['posting'] = Posting::find($id);
        $data['action'] = url('posting/update'). '/' .$data['posting']->id;
        $data['tombol'] = 'Update';

        return view('posting.from',$data);
     }

     function update(Request $req){
        Posting::where('id',$req->id)->update([
            'nama' =>$req->nama,
            'bidang_usaha' =>$req->bidang_usaha,
            'persyaratan' =>$req->persyaratan,
            'lowongan' =>$req->lowongan,
            'ttl_p' =>$req->ttl_p,
            'ttl_tp' =>$req->ttl_tp,
            'deskripsi' =>$req->deskripsi,
            'foto' =>$req->foto,
            'lokasi' =>$req->lokasi,
        ]);
        return redirect('posting.index');
     }
}
