<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    //
    function show(){
        return view('layout.dashboard');
    }
     function admin(){
        return view('layout.admin');
     }

     function from(){
        return view('posting.from');
     }
     function index(){
      return view('posting.index');
   }
}
