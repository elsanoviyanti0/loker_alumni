<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Auth;


class User1Controller extends Controller
{
    //
    function show(){
        return view('footer.daftar');
    }

    function view(){
        return view('footer.login');
    }

    function create(Request $req){
        $validate = $this->validate($req,[
            'nisn' => 'required|string',
            'name' => 'required|string',
            'ktp' => 'required|string|min:10',
            'tanggal_lahir' => 'required|date',
            'alamat' => 'required|string',
            'no_hp' => 'required|string',
            'ttn_lulus' => 'required|string',
            'foto' => 'required|file',
            'email' => 'string|string|max:4',
            'password' => 'required_with:confirm_password|min:8|same:confirm_password',

        ]);

        $validate['password'] = bcrypt($req->password);

        User::create($validate);

        return redirect('footer.login');
    }

    function userupdate(Request $req, $id){
        User::Where('id', $id)->update([
            'nisn' => $req->nisn,
            'name' => $req->name,
            'ktp' => $req->ktp,
            'tanggal_lahir' => $req->tanggal_lahir,
            'alamat' => $req->alamat,
            'no_hp' => $req->no_hp,
            'ttn_lulus' => $req->ttn_lulus,
            'foto' => $req->foto,
            'email' => $req->email,
            'password' => $req->password
        ]);

        return redirect('myuser');
    }

    function auth(Request $req){
        $credentials = $req->only('email','password');

        if (Auth::attempt($credentials)) {

            if (Auth::user()->level === "admin") {
                return redirect('stisla');

            }else if (Auth::user()->level  === "alumni"){
                return redirect('beranda');
            }

        }
        // return redirect()->back();
    }

    function logout(){
        Auth::logout();
        return redirect('footer.login');
    }
}
