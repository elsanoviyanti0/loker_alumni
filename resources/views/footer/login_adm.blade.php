@extends('auth')

@section('title', 'Alumni-Login-Admin')

@section('content')
    <section>
        <div class="form-box">
            <div class="form-value">
                <form action="">
                  <!-- <img src="img/chat.png" alt="100"  width="100" height="100"> -->
                    <div class="login-group">
                        <div class="login-count"><img src="img/chat.png" alt="100"  width="100" height="100"></div>
                        <h2>LOGIN</h2>
                    <form action="{{ url('/login/admin') }}" method="post">
                     @csrf
                    
                    <div class="inputbox">
                        <ion-icon name="mail-outline"></ion-icon>
                        <input type="email" required>
                        <label for="">Email</label>
                    </div>
                    <div class="inputbox">
                        <ion-icon name="lock-closed-outline"></ion-icon>
                        <input type="password" required>
                        <label for="">Password</label>
                    </div>
                    <!-- <div class="forget">
                        <label for=""><input type="checkbox">Remember Me</label> <a href="#">Forget Password</a>
                       
                    </div> -->
                    <button>Log In</button>
                    <div class="register">
                        <p>Don't have a account</p><a href="#">Register</a>
                    </div>
                </form>
            </div>
        </div>
    </section>
    <script type="module" src="https://unpkg.com/ionicons@7.1.0/dist/ionicons/ionicons.esm.js"></script>
    <script nomodule src="https://unpkg.com/ionicons@7.1.0/dist/ionicons/ionicons.js"></script>
 @endsection
