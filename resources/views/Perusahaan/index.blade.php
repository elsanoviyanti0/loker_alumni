<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Perusahaan</title>
    <link rel="stylesheet" href="{{asset('plugin/css/dsh.css')}}">
        <!-- plugins:css -->
        <link rel="stylesheet" href="../../lgn/assets/vendors/mdi/css/materialdesignicons.min.css">
        <link rel="stylesheet" href="../../lgn/assets/vendors/css/vendor.bundle.base.css">
        <link href="https://cdn.jsdelivr.net/npm/simple-datatables@7.1.2/dist/style.min.css" rel="stylesheet" />
        <script src="https://use.fontawesome.com/releases/v6.3.0/js/all.js" crossorigin="anonymous"></script>
        <!-- endinject -->
        <!-- Plugin css for this page -->
        <!-- End plugin css for this page -->
        <!-- inject:css -->
        <!-- endinject -->
        <!-- Layout styles -->
        <link rel="stylesheet" href="../../lgn/assets/css/style.css">
        <!-- End layout styles -->
        <link rel="shortcut icon" href="../../lgn/assets/images/logo smk.png" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.0/css/all.min.css">
</head>
<body>
    <div class="sidebar" style="background: rgb(45, 85, 215)">
        <div class="logo"></div>
        <ul class="menu">
            <li class="active">
                <a href="#">
                    <i class="fas fa-tachometer-alt"></i>
                    <span>Dashboard</span>
                </a>
            </li>
                <li>
                <a href="#">
                    <i class="fas fa-user"></i>
                    <span>Profile</span>
                </a>
            </li>
            <li>
                <a href="#">
                    <i class="fas fa-chart-bar"></i>
                    <span>Statistics</span>
                </a>
            </li>
            <li>
                <a href="#">
                    <i class="fas fa-briefcase"></i>
                    <span>Careers</span>
                </a>
            </li>
            <li>
                <a href="#">
                    <i class="fas fa-question-circle"></i>
                    <span>Dashboard</span>
                </a>
            </li>
            <li>
                <a href="#">
                    <i class="fas fa-tachometer-alt"></i>
                    <span>Faq</span>
                </a>
            </li>
            <li>
                <a href="#">
                    <i class="fas fa-star"></i>
                    <span>Testimonials</span>
                </a>
            </li>
            <li>
                <a href="#">
                    <i class="fas fa-cog"></i>
                    <span>Setting</span>
                </a>
            </li>
            <li class="logout">
                <a href="#">
                    <i class="fas fa-sign-out-alt"></i>
                    <span>Logout</span>
                </a>
            </li>
        </ul>
    </div>
    <div class="main--content">
        <div class="header--wrapper">
            <div class="hidden--title">
                <span>Primary</span>
                <h2>Dashboard</h2>
            </div>
            <div class="user--info">
                <div class="searh--box">
                    <i class="fa-solid fa-search"></i>
                    <input type="text" name="" id="" placeholder="Search">
                </div>
                <img src="img/logo.png" alt="">
            </div>
        </div>
        <div class="main-panel">
            <div class="content-wrapper" style="background: rgb(235, 237, 240)">
              <div class="page-header">
                {{-- <h3 class="page-title" > Form elements </h3> --}}
                <nav aria-label="breadcrumb">
                  </ol>
                </nav>
              </div>
              <div class="row">
                  <div class="card">
                  </div>
                </div>
                <div class="col-12 grid-margin stretch-card">
                  <div class="card">
                    <div class="card-body">
                      {{-- <h4 class="card-title">Basic form elements</h4>
                      <p class="card-description"> Basic form elements </p> --}}
                      <div id="layoutSidenav_content">
                        <main>
                            <div class="container-fluid px-4">
                                <h1 class="mt-4"> Data Perusahaan</h1>
                                <ol class="breadcrumb mb-4">
                                    <li class="breadcrumb-item active"></li>
                                </ol>
                                <div class="card mb-4">
                                    <div class="card-header">
                                        <i class="fas fa-table me-1"></i>
                                        Data Perusahaan
                                    </div>
                                    <div class="card-body">
                                        <a class="" href="{{ url('perusahaan/add')}}"><button class="btn btn-primary" style="background: blue">Tambah Perusahaan</button></a>
                                        @csrf
                                        <table id="datatablesSimple">
        
                                            <thead>
                                                {{-- <tr>
                                                    <th>NO</th>
                                                    <th>Nama Perusahaan</th>
                                                    <th>Deskripsi</th>
                                                    <th>Foto</th>
                                                    <th>Aksi</th>
                                                </tr> --}}
                                            </thead>
                                            <tfoot style="background: rgb(225, 113, 113)">
                                                <tr>
                                                    <th>NO</th>
                                                    <th>Nama Prusahaan</th>
                                                    <th>Deskripsi</th>
                                                    <th>Foto</th>
                                                    <th>Aksi</th>
                                                </tr>
                                            </tfoot>
                                            <tbody>
                                                @foreach ($perusahaan as $key => $item)
                                                <tr>
                                                    <td>{{ $key+1 }}</td>
                                                    <td>{{ $item->nama_perusahaan }}</td>
                                                    <td>{{ $item->deskripsi }}</td>
                                                    <td>
                                                        <img src="/storage/{{ $item->foto }}" width="50" alt="">
                                                    </td>
                                                    <td>
                                                        <a href="perusahaan/hapus/{{ $item->id }}" class="btn btn-danger btn-sm"><i class="bi bi-trash"></i>Delete</a>
                                                        <a href="perusahaan/edit/{{ $item->id }}" class="btn btn-success btn-sm">Edit</a>
                                                    </td>
                                                    </tr>
                                                @endforeach                                                
                                            </tbody>
                                        </table>
                                    </div>                             
                                </div>
                            </div>
                        </main>
                        <footer class="py-4 bg-light mt-auto">
                            <div class="container-fluid px-4">
                                <div class="d-flex align-items-center justify-content-between small">
                                    <div class="text-muted">Elsa  2023</div>
                                </div>
                            </div>
                        </footer>
                    </div>
                    </div>
                  </div>
                </div>
                </div>
              </div>
            </div>
       <!-- plugins:js -->
       <script src="../../lgn/assets/vendors/js/vendor.bundle.base.js"></script>
       <!-- endinject -->
       <!-- Plugin js for this page -->
       <!-- End plugin js for this page -->
       <!-- inject:js -->
       <script src="../../lgn/assets/js/off-canvas.js"></script>
       <script src="../../lgn/assets/js/hoverable-collapse.js"></script>
       <script src="../../lgnassets/js/misc.js"></script>
       <!-- endinject -->
       <!-- Custom js for this page -->
       <script src="../../lgn/assets/js/file-upload.js"></script>
       <!-- End custom js for this page -->

       <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
       <script src="js/scripts.js"></script>
       <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.js" crossorigin="anonymous"></script>
       <script src="assets/demo/chart-area-demo.js"></script>
       <script src="assets/demo/chart-bar-demo.js"></script>
       <script src="https://cdn.jsdelivr.net/npm/simple-datatables@7.1.2/dist/umd/simple-datatables.min.js" crossorigin="anonymous"></script>
       <script src="js/datatables-simple-demo.js"></script>
</body>
</html>