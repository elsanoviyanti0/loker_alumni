<!-- Main Footer -->
<footer class="main-footer">
    <!-- To the right -->
    <div class="float-right d-none d-sm-inline">
        <p>Created By <a href="https://www.priludestudio.com/" target="blank"><span class="text-primary">SMK YPC Tasikmalaya</span></a></p>
    </div>
    <!-- Default to the left -->
    <strong> Elsa Noviyanti <?= date('Y') ?> <a href="javascript:void(0)">Alumni</a>.</strong> All rights reserved.
</footer>