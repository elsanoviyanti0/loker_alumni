<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Perusahaan;

class PerusahaanController extends Controller
{
    //
    function show(){
        $data['perusahaan'] = Perusahaan::all();
        return view('Perusahaan.index',$data);
    }

    function add(){
        $data=[
            'action'=>url('perusahaan/create'),
            'tombol'=>'Simpan',
            'perusahaan'=>(object)[
                'nama_perusahaan'=>'',
                'deskripsi'=>'',
                'foto'=>'',
            ]
            ];
            return view('Perusahaan.from',$data);
    }

    function create(Request $req){
        Perusahaan::create([
            'nama_perusahaan'=>$req->nama_perusahaan,
            'deskripsi'=>$req->deskripsi,
            'foto'=>$req->foto,
        ]);
        return redirect('Perusahaan.index');
    }

    function hapus($id){
        $perusahaan = Perusahaan::where('id',$id)->delete();
        return redirect('Perusahaan.index');
    }

    function edit($id){
        $data['perusahaan'] = Perusahaan::find($id);
        $data['action'] = url('perusahaan/update'). '/' .$data['perusahaan']->id;
        $data['tombol'] = 'Update';

        return view('Perusahaan.from',$data);
    }

    function update(Request $req){
        Perusahaan::where('id',$req->id)->update([
            'nama_perusahaan'=>$req->nama_perusahaan,
            'deskripsi'=>$req->deskripsi,
            'foto'=>$req->foto,
        ]);
        return redirect('Perusahaan.index');
    }
}
