<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Perusahaan</title>
    <link rel="stylesheet" href="{{asset('plugin/css/dsh.css')}}">
        <!-- plugins:css -->
        <link rel="stylesheet" href="../../lgn/assets/vendors/mdi/css/materialdesignicons.min.css">
        <link rel="stylesheet" href="../../lgn/assets/vendors/css/vendor.bundle.base.css">
        <!-- endinject -->
        <!-- Plugin css for this page -->
        <!-- End plugin css for this page -->
        <!-- inject:css -->
        <!-- endinject -->
        <!-- Layout styles -->
        <link rel="stylesheet" href="../../lgn/assets/css/style.css">
        <!-- End layout styles -->
        <link rel="shortcut icon" href="../../lgn/assets/images/logo smk.png" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.0/css/all.min.css">
</head>
<body>
    <div class="sidebar" style="background: rgb(45, 85, 215)">
        <div class="logo"></div>
        <ul class="menu">
            <li class="active">
                <a href="#">
                    <i class="fas fa-tachometer-alt"></i>
                    <span>Dashboard</span>
                </a>
            </li>
                <li>
                <a href="#">
                    <i class="fas fa-user"></i>
                    <span>Profile</span>
                </a>
            </li>
            <li>
                <a href="#">
                    <i class="fas fa-chart-bar"></i>
                    <span>Statistics</span>
                </a>
            </li>
            <li>
                <a href="#">
                    <i class="fas fa-briefcase"></i>
                    <span>Careers</span>
                </a>
            </li>
            <li>
                <a href="#">
                    <i class="fas fa-question-circle"></i>
                    <span>Dashboard</span>
                </a>
            </li>
            <li>
                <a href="#">
                    <i class="fas fa-tachometer-alt"></i>
                    <span>Faq</span>
                </a>
            </li>
            <li>
                <a href="#">
                    <i class="fas fa-star"></i>
                    <span>Testimonials</span>
                </a>
            </li>
            <li>
                <a href="#">
                    <i class="fas fa-cog"></i>
                    <span>Setting</span>
                </a>
            </li>
            <li class="logout">
                <a href="#">
                    <i class="fas fa-sign-out-alt"></i>
                    <span>Logout</span>
                </a>
            </li>
        </ul>
    </div>
    <div class="main--content">
        <div class="header--wrapper">
            <div class="hidden--title">
                <span>Primary</span>
                <h2>Dashboard</h2>
            </div>
            <div class="user--info">
                <div class="searh--box">
                    <i class="fa-solid fa-search"></i>
                    <input type="text" name="" id="" placeholder="Search">
                </div>
                <img src="img/logo.png" alt="">
            </div>
        </div>
        <div class="main-panel">
            <div class="content-wrapper" style="background: rgb(235, 237, 240)">
              <div class="page-header">
                <h3 class="page-title" > Form elements </h3>
                <nav aria-label="breadcrumb">
                  <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">Forms</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Form elements</li>
                  </ol>
                </nav>
              </div>
              <div class="row">
                  <div class="card">
                  </div>
                </div>
                <div class="col-12 grid-margin stretch-card">
                  <div class="card">
                    <div class="card-body">
                     <form action="{{ $action }}" method="post" enctype="multipart/form-data">
                        @csrf
                      {{-- <h4 class="card-title">Basic form elements</h4>
                      <p class="card-description"> Basic form elements </p> --}}
                      <form class="forms-sample">
                        <div class="form-group">
                          <label for="exampleInputName1">Nama Perusahaan</label>
                          <input type="text" class="form-control" id="exampleInputName1" placeholder="Nama Perusahaan">
                        </div>
                        <div class="form-group">
                            <label for="exampleTextarea1">Deskripsi</label>
                            <textarea class="form-control" id="exampleTextarea1" rows="4"></textarea>
                          </div>
                        <div class="form-group">
                          <label>File upload</label>
                          <input type="file" name="img[]" class="file-upload-default">
                          <div class="input-group col-xs-12">
                            <input type="text" class="form-control file-upload-info" disabled placeholder="Upload Image">
                            <span class="input-group-append">
                              <button class="file-upload-browse btn btn-gradient-primary" type="button" style="background: rgb(45, 85, 215)">Upload</button>
                            </span>
                          </div>
                        </div>
                        <button type="submit" class="btn btn-gradient-primary me-2" style="background: rgb(45, 85, 215)">Submit</button>
                        <button class="btn btn-light me-2" style="background: rgb(236, 81, 81)" style="color: white">Cancel</button>
                      </form>
                    </div>
                  </div>
                </div>
                </div>
              </div>
            </div>
       <!-- plugins:js -->
       <script src="../../lgn/assets/vendors/js/vendor.bundle.base.js"></script>
       <!-- endinject -->
       <!-- Plugin js for this page -->
       <!-- End plugin js for this page -->
       <!-- inject:js -->
       <script src="../../lgn/assets/js/off-canvas.js"></script>
       <script src="../../lgn/assets/js/hoverable-collapse.js"></script>
       <script src="../../lgnassets/js/misc.js"></script>
       <!-- endinject -->
       <!-- Custom js for this page -->
       <script src="../../lgn/assets/js/file-upload.js"></script>
       <!-- End custom js for this page -->
</body>
</html>