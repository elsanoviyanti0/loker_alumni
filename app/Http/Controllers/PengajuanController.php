<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Pengajuan;

class PengajuanController extends Controller
{
    //
    function show(){
        $data['pengajuan'] = Pengajuan::all();
        return view('pengajuan.index',$data);
    }

    function adm(){
        return view('pengajuan.from');
    }

    function add(){
        $data=[
            'action' =>url('pengajuan/create'),
            'tombol' =>'Simpan',
            'pengajuan' =>(object)[
                'nama_lengkap' =>'',
                'ttl' =>'',
                'tempat' =>'',
                'alamat' =>'',
                'no_hp' =>'',
                'email' =>'',
                'ttl_p' =>'',
                'foto' =>'',
                'dokumen' =>'',
            ]
            ];
            return view('pengajuan.from',$data);
    }

    function create(Request $req){
        Pengajuan::create([
            'nama_lengkap' =>$req->nama_lengkap,
            'ttl' =>$req->ttl,
            'tempat' =>$req->tempat,
            'alamat' =>$req->alamat,
            'no_hp' =>$req->no_hp,
            'email' =>$req->email,
            'ttl_p' =>$req->ttl_p,
            'foto' =>$req->foto,
            'dokumen' =>$req->dekumen,
        ]);
        return redirect('pengajuan.index');
    }

    function hapus($id){
        $pengajuan = Pengajuan::where('id',$id)->delete();
        return redirect('pengajuan.index');
    }
     
    function edit($id){
        $data['pengajuan'] = Pengajuan::find($id);
        $data['action'] = url('pengajuan/update'). '/' .$data['pengajuan']->id;
        $data['tombol'] = 'Update';

        return view('pengajuan.from', $data);
    }
     
    function update(Request $req){
        Pengajuan::where('id',$req->id)->update([
            'nama_lengkap' =>$req->nama_lengkap,
            'ttl' =>$req->ttl,
            'tempat' =>$req->tempat,
            'alamat' =>$req->alamat,
            'no_hp' =>$req->no_hp,
            'email' =>$req->email,
            'ttl_p' =>$req->ttl_p,
            'foto' =>$req->foto,
            'dokumen' =>$req->dekumen, 
        ]);

        return redirect('pengajuan.index');
    }
}
